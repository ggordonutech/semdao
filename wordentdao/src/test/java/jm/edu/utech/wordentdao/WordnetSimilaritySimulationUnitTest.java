package jm.edu.utech.wordentdao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class WordnetSimilaritySimulationUnitTest {
    @Test
    public void shouldRunSimilarityTests(){
        System.out.println("Wordnet Test running");
        String[] results = WordnetSimilaritySimulation.testRun("car","bus");
        assertTrue(results != null);
    }
}