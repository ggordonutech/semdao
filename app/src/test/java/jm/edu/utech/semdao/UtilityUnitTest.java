package jm.edu.utech.semdao;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilityUnitTest {
    @Test
    public void shouldAddTwoNumbers() {
        int num1 = 4,num2 = 5;
        int result = Utility.addTwoNumbers(num1,num2);
        assertEquals(9, result);
    }
}
