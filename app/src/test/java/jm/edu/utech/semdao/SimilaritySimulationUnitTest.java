package jm.edu.utech.semdao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class SimilaritySimulationUnitTest {
    @Test
    public void shouldRunSimilarityTests(){
        System.out.println("Test running");
        String[] results = SimilaritySimulation.testRun("car","bus");
        assertTrue(results != null);
    }
}
