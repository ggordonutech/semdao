package jm.edu.utech.semdao;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void shouldAddTwoNumbers() {
        int num1 = 4,num2 = 5;
        int result = Utility.addTwoNumbers(num1,num2);
        assertEquals(9, result);
    }
}