package jm.edu.utech.semdao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtPhrase1;
    private EditText txtPhrase2;
    private EditText txtSimilarityScores;
    private Button btnFindSimilarity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtPhrase1 = (EditText) findViewById(R.id.txtPhrase1);
        txtPhrase2 = (EditText) findViewById(R.id.txtPhrase2);
        txtSimilarityScores = (EditText) findViewById(R.id.txtSimilarityScores);
        btnFindSimilarity = (Button)findViewById(R.id.btnFindSimilarity);

        btnFindSimilarity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findSimilarityScoresAndPopuplateResultsArea();
            }
        });
    }

    protected void findSimilarityScoresAndPopuplateResultsArea(){
        try {
            String[] results = SimilaritySimulation.testRun(
                    txtPhrase1.getText().toString(),
                    txtPhrase2.getText().toString()
            );
            String resultsAsString = "";
            for (String line : results) {
                resultsAsString += line + "\n";
            }
            txtSimilarityScores.setText(resultsAsString);
            Toast.makeText(this, "Similarity Scores included", Toast.LENGTH_SHORT);
        }catch(Exception e){
            Log.e("Similarity error","Unableto retrieve similarity",e);
            Toast.makeText(this, "Error : "+e.getMessage(), Toast.LENGTH_LONG);
        }
    }
}
