package jm.edu.utech.semdao;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import jm.edu.utech.wordentdao.AndroidWordnetLexicalDatabase;

public class SimilaritySimulation {
//    private static ILexicalDatabase db = new NictWordNet();
    private static ILexicalDatabase db = new AndroidWordnetLexicalDatabase();
    private static RelatednessCalculator[] rcs = {
            /**
             * These needs to be re-implemented since the edu.cmu.lti.ws4j.util.Traverser uses the edu.cmu.lti.jawjaw.db.SynlinkDAO
             * */
//            new HirstStOnge(db),
            new LeacockChodorow(db), new Lesk(db),
            new WuPalmer(db),
            new Resnik(db), new JiangConrath(db), new Lin(db), new Path(db)
    };

    public static String[] testRun( String word1, String word2 ) {
        WS4JConfiguration.getInstance().setMFS(true);
        String[] results = new String[rcs.length];
        int index=0;
        for ( RelatednessCalculator rc : rcs ) {
            double s = rc.calcRelatednessOfWords(word1, word2);
            String message = rc.getClass().getName()+"\t"+s;
            System.out.println( message );
            results[index++]=message;
        }
        return  results;
    }

    private static void run( String word1, String word2 ) {
        WS4JConfiguration.getInstance().setMFS(true);
        for ( RelatednessCalculator rc : rcs ) {
            double s = rc.calcRelatednessOfWords(word1, word2);
            System.out.println( rc.getClass().getName()+"\t"+s );
        }
    }
    public static void main(String[] args) {
        long t0 = System.currentTimeMillis();
        run( "act","moderate" );
        long t1 = System.currentTimeMillis();
        System.out.println( "Done in "+(t1-t0)+" msec." );
    }
}
